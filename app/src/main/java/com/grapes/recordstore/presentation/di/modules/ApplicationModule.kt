package com.grapes.recordstore.presentation.di.modules

import android.content.Context
import com.grapes.recordstore.presentation.di.qualifiers.ForApplication
import com.grapes.recordstore.presentation.myApp.MyApplication
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

  @Provides
  @ForApplication
  fun applicationContext(app: MyApplication): Context {
    return app.applicationContext
  }

}