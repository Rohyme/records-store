package com.grapes.recordstore.presentation.ui.screens.mainScreen

import android.arch.lifecycle.ViewModel
import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.databinding.ObservableInt
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import com.grapes.recordstore.R
import com.grapes.recordstore.databinding.ActivityMainBinding
import com.grapes.recordstore.presentation.ui.base.BaseActivityWithInjector
import com.grapes.recordstore.presentation.ui.screens.favouriteScreen.FavouriteScreenFragment
import com.grapes.recordstore.presentation.ui.screens.homeScreen.HomeScreenFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivityWithInjector() {

    val bucketListener = ObservableInt(0)

    override fun getActivityVM(): Class<out ViewModel> {
        return MainActivityViewModel::class.java
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = Color.WHITE
        }

        favouriteBt.setOnClickListener {
            openFragment(FavouriteScreenFragment(), FavouriteScreenFragment::class.java.name)
        }
        openFragment(HomeScreenFragment(), HomeScreenFragment.Tag)
        bucketListener()
        if (bucketListener.get() == 0) {
            bucketCount.visibility = View.GONE
        }
        binding.bucketCounter = bucketListener
    }

    fun bucketListener() {
        bucketListener.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (bucketListener.get() == 0) {
                    bucketCount.visibility = View.GONE
                } else {
                    bucketCount.visibility = View.VISIBLE
                    bucketCount.text = "${bucketListener.get()}"
                }
            }
        })
    }

    fun bucketIncrease(increasedBy: Int) {
        bucketListener.set(bucketListener.get() + increasedBy)
    }

    fun openFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction().addToBackStack(null)
                .replace(R.id.fragmentContainer, fragment, tag)
                .commit()
    }

    fun getFavView(): View {
        return favouriteBt
    }

    fun getBucketView(): View {
        return bucketBt
    }


    override fun onBackPressed() {
        val fm = supportFragmentManager
        if (fm.backStackEntryCount > 1 && supportFragmentManager.findFragmentByTag(HomeScreenFragment.Tag) != null) {
            Log.i("MainActivity", "popping backstack")
            fm.popBackStack()
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super")
            finish()
        }
    }

}
