package com.grapes.recordstore.presentation.ui.screens.favouriteScreen

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blankj.utilcode.util.ToastUtils
import com.grapes.recordstore.R
import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteModel
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import com.grapes.recordstore.databinding.FragmentFavouriteScreenBinding
import com.grapes.recordstore.databinding.ListItemFavouriteBinding
import com.grapes.recordstore.presentation.SortTripleState
import com.grapes.recordstore.presentation.appUtils.SortConstants
import com.grapes.recordstore.presentation.appUtils.StateView
import com.grapes.recordstore.presentation.ui.base.BaseFragmentWithInjector
import com.grapes.recordstore.presentation.ui.screens.mainScreen.MainActivity
import com.tripl3dev.luffyyview.baseAdapter.BaseListAdapter
import com.tripl3dev.luffyyview.baseAdapter.MainHolderInterface
import com.tripl3dev.prettystates.StatesConstants
import com.tripl3dev.prettystates.setState
import kotlinx.android.synthetic.main.fragment_favourite_screen.*

// Created by Rohyme on 10/24/2018.
class FavouriteScreenFragment : BaseFragmentWithInjector(), SortTripleState.SortListener {

    lateinit var binding: FragmentFavouriteScreenBinding
    lateinit var viewModel: FavouriteScreenVM
    lateinit var mAdapter: BaseListAdapter<FavouriteModel>

    override fun getActivityVM(): Class<out ViewModel> {
        return FavouriteScreenVM::class.java
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = vm as FavouriteScreenVM
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favourite_screen, container, false)
        binding.backButton.setOnClickListener {
            if (activity is MainActivity) {
                (activity as MainActivity).onBackPressed()
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.sorted.name.setSortListener(this)
        binding.sorted.priceSorting.setSortListener(this)
        binding.sorted.releaseDate.setSortListener(this)
        setUpFavouriteList()
        setFetchFavListener()
        toggleFavListener()
        viewModel.fetchFavourite()

    }

    override fun onStop() {
        super.onStop()
        isSorted = false
    }

    /**
     * Set up Favourite List
     */
    private fun setUpFavouriteList() {
        mAdapter = BaseListAdapter(object : MainHolderInterface<FavouriteModel> {
            override fun getView(type: Int): Int {
                return R.layout.list_item_favourite
            }

            override fun getViewData(holder: RecyclerView.ViewHolder, t: FavouriteModel, position: Int) {
                val itemBinding = DataBindingUtil.bind<ListItemFavouriteBinding>(holder.itemView)
                itemBinding!!.model = t
                itemBinding.removeFromFavBt.setOnClickListener {
                    viewModel.toggleFav(t.id)
                }
            }
        }, context!!)

        favouritesList.layoutManager = LinearLayoutManager(context!!)
        favouritesList.adapter = mAdapter
    }

    /**
     * set Up Favourite useCases Listeners
     */

    private fun toggleFavListener() {
        viewModel.getToggleListener().observe(this, Observer { state ->
            when (state) {
                is StateView.Success<*> -> {
                    val favToDelete = state.data as String
                    val itemToDelete = mAdapter.originalList.find {
                        it.id == favToDelete
                    }
                    val index = mAdapter.originalList.indexOf(itemToDelete)
                    mAdapter.originalList.removeAt(index)
                    mAdapter.notifyItemRemoved(index)
                }

                is StateView.Errors -> {
                    ToastUtils.showShort(state.error?.message.toString())
                }
            }
        })
    }

    private fun setFetchFavListener() {
        viewModel.getFavListener().observe(this, Observer {
            when (it) {
                is StateView.Success<*> -> {
                    favouritesList.setState(StatesConstants.NORMAL_STATE)
                    val favList = it.data as ArrayList<FavouriteModel>
                    mAdapter.originalList.clear()
                    mAdapter.originalList = favList
                    mAdapter.notifyDataSetChanged()
                }
                is StateView.Loading -> {
                    favouritesList.setState(StatesConstants.LOADING_STATE)
                }
                is StateView.Errors -> {
                    ToastUtils.showShort(it.error?.message.toString())
                    favouritesList.setState(StatesConstants.ERROR_STATE)
                            .setOnClickListener {
                                if (isSorted)
                                    viewModel.retryFetchingSortedFavourite()
                                else
                                    viewModel.retryFetchingFavourite()
                            }
                }
                is StateView.Empty -> {
                    favouritesList.setState(StatesConstants.EMPTY_STATE)
                }
            }
        })
    }


    /**
     * Sorting Views listeners
     */

    var isSorted = false

    override fun onUpSelected(v: View) {
        sort(v.id, true)
    }

    override fun onDownSelected(v: View) {
        sort(v.id, false)
    }

    override fun onOffSelected(v: View) {
    }

    fun sort(viewId: Int, isUp: Boolean) {
        isSorted = true
        setOtherSortingToOff(viewId)
        val sortType = when (viewId) {
            R.id.name -> {
                SortConstants.NAME_SORTING
            }
            R.id.priceSorting -> {
                SortConstants.PRICE_SORTING
            }
            R.id.releaseDate -> {
                SortConstants.DATE_SORTING
            }
            else -> {
                SortConstants.NAME_SORTING
            }
        }
        viewModel.fetchSortedFavourite(SortedRecordsRequest(1, sortType, !isUp))
    }

    fun setOtherSortingToOff(viewId: Int) {
        val listOfSorting = arrayOf(R.id.name, R.id.releaseDate, R.id.priceSorting)
        listOfSorting.filter {
            it != viewId
        }.forEach {
            val v = view?.findViewById<SortTripleState>(it)
            v?.executeOffState()
        }
    }


}