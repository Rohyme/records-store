package com.grapes.recordstore.presentation.di.modules

import android.arch.persistence.room.Room
import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteDao
import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteDataBase
import com.grapes.recordstore.presentation.myApp.MyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

// Created by Rohyme on 10/24/2018.
@Module
class DataBaseModule{

    @Provides
    @Singleton
    fun provideMoviesDao(appContext: MyApplication): FavouriteDao{
        return Room.databaseBuilder(appContext, FavouriteDataBase::class.java, "favourite_db").fallbackToDestructiveMigration()
                .build().favouriteDao()
    }
}