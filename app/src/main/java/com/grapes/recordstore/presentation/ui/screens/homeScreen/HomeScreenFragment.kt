package com.grapes.recordstore.presentation.ui.screens.homeScreen

import android.animation.Animator
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.blankj.utilcode.util.ToastUtils
import com.grapes.recordstore.R
import com.grapes.recordstore.databinding.FragmentMainScreenBinding
import com.grapes.recordstore.databinding.ListItemHomeRecordsBinding
import com.grapes.recordstore.domain.entities.RecordModel
import com.grapes.recordstore.domain.entities.ToggleModel
import com.grapes.recordstore.presentation.SortTripleState
import com.grapes.recordstore.presentation.appUtils.StateView
import com.grapes.recordstore.presentation.appUtils.viewUtils.CircleAnimationUtil
import com.grapes.recordstore.presentation.ui.base.BaseFragmentWithInjector
import com.grapes.recordstore.presentation.ui.screens.mainScreen.MainActivity
import com.grapes.recordstore.presentation.ui.screens.recordDetailsScreen.RecordsDetailsFragment
import com.tripl3dev.luffyyview.baseAdapter.BaseListAdapter
import com.tripl3dev.luffyyview.baseAdapter.MainHolderInterface
import com.tripl3dev.prettystates.StatesConstants
import com.tripl3dev.prettystates.setState
import kotlinx.android.synthetic.main.fragment_main_screen.*
import ru.alexbykov.nopaginate.paginate.NoPaginate


// Created by Rohyme on 10/23/2018.
class HomeScreenFragment : BaseFragmentWithInjector(), SortTripleState.SortListener {
    lateinit var mAdapter: BaseListAdapter<RecordModel>
    lateinit var viewModel: HomeScreenVM
    lateinit var paginate: NoPaginate
    lateinit var binding: FragmentMainScreenBinding
    var currentOrder = false
    var currentSort = ""

    override fun getActivityVM(): Class<out ViewModel> {
        return HomeScreenVM::class.java
    }

    companion object {
        val Tag = "HomeScreenFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = vm as HomeScreenVM
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_screen, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.sortingBar.name.setSortListener(this)
        binding.sortingBar.priceSorting.setSortListener(this)
        binding.sortingBar.releaseDate.setSortListener(this)
        setUpRecordsList()
        setRecordsListener()
        setAddToFavListener()

    }

    override fun onResume() {
        super.onResume()
        resetPaginate()
        viewModel.fetchRecords()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            paginate.unbind()
        } catch (e: Exception) {
            Log.e("paginate", "notRegistered")
        }
    }

    /**
     * set Listeners
     */

    private fun setAddToFavListener() {
        viewModel.getAddToFavListener().observe(this, Observer {
            when (it) {
                is StateView.Success<*> -> {
                    val toggleModel = it.data as ToggleModel
                    val record = mAdapter.originalList.find { recordModel ->
                        recordModel.id == toggleModel.id
                    }
                    val index = mAdapter.originalList.indexOf(record)
                    mAdapter.originalList[index].isFavourite = toggleModel.status
                    mAdapter.notifyItemChanged(index)
                }
                is StateView.Errors -> {
                    ToastUtils.showShort(it.error?.message.toString())
                }
            }
        })
    }


    private fun setRecordsListener() {
        viewModel.getRecordsListener().observe(activity!!, Observer {
            when (it) {
                is StateView.Success<*> -> {
                    val list = it.data as ArrayList<RecordModel>
                    recordsList.setState(StatesConstants.NORMAL_STATE)
                    mAdapter.originalList.clear()
                    mAdapter.originalList.addAll(list)
                    mAdapter.notifyDataSetChanged()
                    viewModel.currentPage = 1
                    endPaginate(false)
                }
                is StateView.PaginationSuccess<*> -> {
                    val list = it.data as ArrayList<RecordModel>
                    val oldSize = mAdapter.originalList.size
                    mAdapter.originalList.addAll(list)
                    mAdapter.notifyItemRangeInserted(oldSize - 1, mAdapter.originalList.size)
                    endPaginate(list.isEmpty())

                }
                is StateView.Loading -> {
                    recordsList.setState(StatesConstants.LOADING_STATE)
                }
                is StateView.PaginationLoading -> {
                    paginate.showLoading(true)
                }
                is StateView.PaginationError -> {
                    paginate.showError(true)
                }
                is StateView.Errors -> {
                    recordsList.setState(StatesConstants.ERROR_STATE)
                            .setOnClickListener {
                                if (isSorted) {
                                    viewModel.retrySorted()
                                } else {
                                    viewModel.retryUnSorted()
                                }
                            }
                }
                is StateView.Empty -> {
                    recordsList.setState(StatesConstants.EMPTY_STATE)
                }
                is StateView.PaginationFinished -> {
                    endPaginate(true)
                }
            }
        })
    }


    /**
     * setUp records Recycler view
     */
    private fun setUpRecordsList() {
        mAdapter = BaseListAdapter(object : MainHolderInterface<RecordModel> {
            override fun getView(type: Int): Int {
                return R.layout.list_item_home_records
            }

            override fun getViewData(holder: RecyclerView.ViewHolder, t: RecordModel, position: Int) {
                val itemBinding = DataBindingUtil.bind<ListItemHomeRecordsBinding>(holder.itemView)
                itemBinding!!.model = t
                itemBinding.executePendingBindings()
                itemBinding.favouriteBt.setOnClickListener {
                    if (t.isFavourite) {
                        viewModel.toggleFav(t.id)
                        ToastUtils.showShort("removing from Favourite ...")
                    } else {
                        viewModel.toggleFav(t.id)
                        ToastUtils.showShort("Adding to Favourite ...")
                        makeFlyAnimation(itemBinding.favouriteBt)
                    }
                }
                itemBinding.playBt.setOnClickListener {
                    (activity as MainActivity).openFragment(RecordsDetailsFragment.instance(t), RecordsDetailsFragment::class.java.name)
                }
                itemBinding.root.setOnClickListener {
                    (activity as MainActivity).openFragment(RecordsDetailsFragment.instance(t), RecordsDetailsFragment::class.java.name)
                }
            }
        }, context!!)
        recordsList.layoutManager = LinearLayoutManager(context)
        recordsList.setHasFixedSize(true)
        recordsList.adapter = mAdapter
        setUpPagination()
    }

    private fun makeFlyAnimation(targetView: ImageButton) {

        val destView = (activity as MainActivity).getFavView()

        CircleAnimationUtil().attachActivity(activity).setTargetView(targetView).setMoveDuration(1000).setDestView(destView).setAnimationListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }


            override fun onAnimationEnd(animation: Animator?) {
            }

            override fun onAnimationCancel(animation: Animator?) {
            }


            override fun onAnimationStart(animation: Animator?) {
            }
        }).startAnimation()
    }


    /**
     * set up Pagination
     */

    private fun setUpPagination() {
        paginate = NoPaginate.with(recordsList)
                .setOnLoadMoreListener {
                    if (!this::paginate.isInitialized) return@setOnLoadMoreListener
                    paginate.showLoading(true)
                    if (viewModel.inPaginate()) {
                        if (isSorted)
                            viewModel.fetchSortedRecords(currentOrder, currentSort)
                        else
                            viewModel.fetchRecords()
                    } else {
                        recordsList.setState(StatesConstants.LOADING_STATE)
                        if (isSorted)
                            viewModel.fetchSortedRecords(currentOrder, currentSort)
                        else
                            viewModel.fetchRecords()
                    }
                }
                .build()
    }

    fun endPaginate(hasNoMoreData: Boolean) {
        when {
            hasNoMoreData -> {
                paginate.setNoMoreItems(true)
                paginate.showLoading(false)
                return
            }
            else -> {
                viewModel.currentPage++
                paginate.showLoading(false)
                paginate.setNoMoreItems(false)
                return
            }
        }
    }

    fun resetPaginate() {
        viewModel.resetPagination()
    }


    /**
     * Sorting Views listeners
     */

    private var isSorted: Boolean = false

    override fun onUpSelected(v: View) {
        setOtherSortingToOff(v.id)
        sort(v.id, true)
    }

    override fun onDownSelected(v: View) {
        setOtherSortingToOff(v.id)
        sort(v.id, false)
    }

    override fun onOffSelected(v: View) {

    }

    fun sort(viewId: Int, isUp: Boolean) {
        val order = if (isUp) "asc" else "desc"
        val sortType = when (viewId) {
            R.id.name -> {
                "name"
            }
            R.id.priceSorting -> {
                "price"
            }
            R.id.releaseDate -> {
                "releaseDate"
            }
            else -> {
                "name"
            }
        }
        currentOrder = isUp
        currentSort = sortType
        viewModel.fetchSortedRecords(isUp, sortType)
    }

    fun setOtherSortingToOff(viewId: Int) {
        resetPaginate()
        isSorted = true
        val listOfSorting = arrayOf(R.id.name, R.id.releaseDate, R.id.priceSorting)
        listOfSorting.filter {
            it != viewId
        }.forEach {
            val v = view?.findViewById<SortTripleState>(it)
            v?.executeOffState()
        }
    }

}