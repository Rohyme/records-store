package com.grapes.recordstore.presentation.ui.screens.favouriteScreen

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.persistence.room.EmptyResultSetException
import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteModel
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import com.grapes.recordstore.domain.useCases.favUseCases.GetFavouritesUseCase
import com.grapes.recordstore.domain.useCases.favUseCases.GetSortedFavouriteUseCase
import com.grapes.recordstore.domain.useCases.favUseCases.ToggleFavouriteUseCase
import com.grapes.recordstore.presentation.appUtils.StateView
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import javax.inject.Inject

// Created by Rohyme on 10/24/2018.
class FavouriteScreenVM @Inject constructor(val toggleFavouriteUseCase: ToggleFavouriteUseCase,
                                            val getFavouritesUseCase: GetFavouritesUseCase,
                                            val getSortedFavUseCase: GetSortedFavouriteUseCase) : ViewModel() {

    private val favListListener = MutableLiveData<StateView>()
    private val toggleFavListener = MutableLiveData<StateView>()


    /**
     * get Listeners
     */
    fun getFavListener():LiveData<StateView>{
        return favListListener
    }
    fun getToggleListener():LiveData<StateView>{
        return toggleFavListener
    }


    /**
     * Making Requests
     */
    fun fetchFavourite() {
        getFavouritesUseCase.execute(getFavObserver())
    }
    fun fetchSortedFavourite(sortedRecordsRequest: SortedRecordsRequest){
        getSortedFavUseCase.execute(getFavObserver(),sortedRecordsRequest)
    }
    fun getFavObserver():SingleObserver<ArrayList<FavouriteModel>>{
        return  object : SingleObserver<ArrayList<FavouriteModel>> {
            override fun onSuccess(t: ArrayList<FavouriteModel>) {
                if (t.isNotEmpty())
                    favListListener.postValue(StateView.Success(t))
                else
                    favListListener.postValue(StateView.Empty)
            }

            override fun onSubscribe(d: Disposable) {
                favListListener.postValue(StateView.Loading)
            }

            override fun onError(e: Throwable) {
                if (e is EmptyResultSetException) {
                    favListListener.postValue(StateView.Empty)
                } else {
                    favListListener.postValue(StateView.Errors(e))
                }
            }
        }
    }
    fun toggleFav(favId :String){
        toggleFavouriteUseCase.execute(object :SingleObserver<Boolean>{
            override fun onSuccess(t: Boolean) {
                toggleFavListener.postValue(StateView.Success(favId))
            }

            override fun onSubscribe(d: Disposable) {
                toggleFavListener.postValue(StateView.Loading)
            }

            override fun onError(e: Throwable) {
                toggleFavListener.postValue(StateView.Errors(e))
            }
        },favId)
    }

    /**
     * retrying requests
     */
    fun retryFetchingFavourite() {
        getFavouritesUseCase.retry()
    }

    fun retryFetchingSortedFavourite(){
        getSortedFavUseCase.retry()
    }

    override fun onCleared() {
        super.onCleared()
        getSortedFavUseCase.unSubscribe()
        toggleFavouriteUseCase.unSubscribe()
        getFavouritesUseCase.unSubscribe()
    }

}