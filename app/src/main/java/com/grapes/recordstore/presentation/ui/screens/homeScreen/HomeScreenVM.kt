package com.grapes.recordstore.presentation.ui.screens.homeScreen

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import com.grapes.recordstore.domain.entities.RecordModel
import com.grapes.recordstore.domain.entities.ToggleModel
import com.grapes.recordstore.domain.useCases.favUseCases.ToggleFavouriteUseCase
import com.grapes.recordstore.domain.useCases.homeUseCases.GetRecordsSortedUseCase
import com.grapes.recordstore.domain.useCases.homeUseCases.GetRecordsUseCase
import com.grapes.recordstore.presentation.appUtils.SingleEventLiveData
import com.grapes.recordstore.presentation.appUtils.StateView
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import javax.inject.Inject

// Created by Rohyme on 10/23/2018.
class HomeScreenVM @Inject constructor(val getRecordsUseCase: GetRecordsUseCase,
                                       val getSortedRecordsUseCase: GetRecordsSortedUseCase,
                                       val addToFavouriteUseCase: ToggleFavouriteUseCase) : ViewModel() {
    var currentPage: Int = 1
    private var recordsListener: SingleEventLiveData<StateView> = SingleEventLiveData()
    private var addToFavListener = SingleEventLiveData<StateView>()
    fun inPaginate() = currentPage != 1


    /**
     * Making requests
     */
    fun fetchRecords() {
        if (getRecordsUseCase.isSubscribe()) return
        getRecordsUseCase.execute(object : SingleObserver<ArrayList<RecordModel>> {
            override fun onSuccess(t: ArrayList<RecordModel>) {
                onSuccessImp(t)
            }

            override fun onSubscribe(d: Disposable) {
                onLoadingImp()
            }

            override fun onError(e: Throwable) {
                onErrorImp(e)
            }
        }, currentPage)
    }

    fun fetchSortedRecords(isUp: Boolean, sortType: String) {
        getSortedRecordsUseCase.execute(object : SingleObserver<ArrayList<RecordModel>> {
            override fun onSuccess(t: ArrayList<RecordModel>) {
                onSuccessImp(t)
            }

            override fun onSubscribe(d: Disposable) {
                onLoadingImp()
            }

            override fun onError(e: Throwable) {
                onErrorImp(e)
            }
        }, SortedRecordsRequest(currentPage, sortType, !isUp))
    }

    fun toggleFav(recordId:String ) {
        addToFavouriteUseCase.execute(object : SingleObserver<Boolean> {
            override fun onSuccess(t: Boolean) {
                addToFavListener.postValue(StateView.Success(ToggleModel(recordId,t)))
            }

            override fun onSubscribe(d: Disposable) {
                addToFavListener.postValue(StateView.Loading)
            }

            override fun onError(e: Throwable) {
                addToFavListener.postValue(StateView.Errors(e))
            }

        }, recordId)
    }





    private fun onErrorImp(e: Throwable) {
        if (inPaginate()) recordsListener.postValue(StateView.PaginationError(e))
        else recordsListener.postValue(StateView.Errors(e))
    }

    private fun onLoadingImp() {
        if (inPaginate()) recordsListener.postValue(StateView.PaginationLoading)
        else recordsListener.postValue(StateView.Loading)
    }


    fun onSuccessImp(t: ArrayList<RecordModel>) {
        if (t.isEmpty()) {
            if (inPaginate()) recordsListener.postValue(StateView.PaginationFinished)
            else recordsListener.postValue(StateView.Empty)
        } else {
            if (inPaginate()) recordsListener.postValue(StateView.PaginationSuccess(t))
            else recordsListener.postValue(StateView.Success(t))
        }
    }

    fun getRecordsListener(): LiveData<StateView> {
        return recordsListener
    }

    fun getAddToFavListener(): LiveData<StateView> {
        return addToFavListener
    }

    fun retryUnSorted() {
        getRecordsUseCase.retry()
    }

    fun retrySorted() {
        getSortedRecordsUseCase.retry()
    }

    fun resetPagination() {
        currentPage = 1
    }

    override fun onCleared() {
        super.onCleared()
        resetPagination()
        getRecordsUseCase.unSubscribe()
        addToFavouriteUseCase.unSubscribe()
        getSortedRecordsUseCase.unSubscribe()
    }



}