package com.grapes.recordstore.presentation.ui.screens.recordDetailsScreen

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.grapes.recordstore.domain.entities.ToggleModel
import com.grapes.recordstore.domain.useCases.favUseCases.ToggleFavouriteUseCase
import com.grapes.recordstore.presentation.appUtils.SingleEventLiveData
import com.grapes.recordstore.presentation.appUtils.StateView
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import javax.inject.Inject

// Created by Rohyme on 10/25/2018.
class RecordsDetailsVM @Inject constructor(val toggleFavouriteUseCase: ToggleFavouriteUseCase) :ViewModel() {
    private var toggleFavListener = SingleEventLiveData<StateView>()

    fun toggleFav(recordId: String){
        toggleFavouriteUseCase.execute(object : SingleObserver<Boolean> {
            override fun onSuccess(t: Boolean) {
                toggleFavListener.postValue(StateView.Success(ToggleModel(recordId,t)))
            }

            override fun onSubscribe(d: Disposable) {
                toggleFavListener.postValue(StateView.Loading)
            }

            override fun onError(e: Throwable) {
                toggleFavListener.postValue(StateView.Errors(e))
            }

        },recordId)
    }

    fun getToggleFavListener():LiveData<StateView>{
        return toggleFavListener
    }

    override fun onCleared() {
        super.onCleared()
        toggleFavouriteUseCase.unSubscribe()
    }

}