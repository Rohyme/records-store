package com.grapes.recordstore.presentation.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.grapes.recordstore.presentation.ui.screens.favouriteScreen.FavouriteScreenVM
import com.grapes.recordstore.presentation.ui.screens.homeScreen.HomeScreenVM
import com.grapes.recordstore.presentation.ui.screens.mainScreen.MainActivityViewModel
import com.grapes.recordstore.presentation.ui.screens.recordDetailsScreen.RecordsDetailsVM
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Singleton
class ViewModelFactory @Inject constructor(private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {

  override fun <T : ViewModel> create(modelClass: Class<T>): T = viewModels[modelClass]?.get() as T
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {

  @Binds
  internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

  @Binds
  @IntoMap
  @ViewModelKey(MainActivityViewModel::class)
  internal abstract fun GuestSessionViewModel(viewModel: MainActivityViewModel): ViewModel


  @Binds
  @IntoMap
  @ViewModelKey(HomeScreenVM::class)
  internal abstract fun HomeScreenVM(viewModel: HomeScreenVM): ViewModel

  @Binds
  @IntoMap
  @ViewModelKey(FavouriteScreenVM::class)
  internal abstract fun FavouriteScreenVM(viewModel: FavouriteScreenVM): ViewModel
  @Binds
  @IntoMap
  @ViewModelKey(RecordsDetailsVM::class)
  internal abstract fun RecordsDetailsVM(viewModel: RecordsDetailsVM): ViewModel


}