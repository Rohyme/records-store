package com.grapes.recordstore.presentation.di.components

import com.grapes.recordstore.presentation.di.modules.ApplicationModule
import com.grapes.recordstore.presentation.di.modules.DataBaseModule
import com.grapes.recordstore.presentation.di.modules.NetworkModule
import com.grapes.recordstore.presentation.di.modules.ViewModelModule
import com.grapes.recordstore.presentation.myApp.MyApplication
import com.grapes.recordstore.presentation.ui.base.BaseActivityWithInjector
import com.grapes.recordstore.presentation.ui.base.BaseFragmentWithInjector
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ApplicationModule::class, NetworkModule::class, ViewModelModule::class , DataBaseModule::class]
)
interface NetworkComponent {
  fun inject(activity: BaseActivityWithInjector)
  fun inject(fragment: BaseFragmentWithInjector)

  @Component.Builder
  interface NetworkBuilder {
    fun builder(): NetworkComponent
    @BindsInstance
    fun application(app: MyApplication): NetworkBuilder
    @BindsInstance
    fun baseUrl(baseUrl: String): NetworkBuilder
  }
}