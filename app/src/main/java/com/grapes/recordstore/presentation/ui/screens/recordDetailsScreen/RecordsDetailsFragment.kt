package com.grapes.recordstore.presentation.ui.screens.recordDetailsScreen

import android.animation.Animator
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blankj.utilcode.util.ToastUtils
import com.grapes.recordstore.R
import com.grapes.recordstore.databinding.FragmentRecordDetailsBinding
import com.grapes.recordstore.domain.entities.RecordModel
import com.grapes.recordstore.domain.entities.ToggleModel
import com.grapes.recordstore.presentation.appUtils.StateView
import com.grapes.recordstore.presentation.appUtils.viewUtils.CircleAnimationUtil
import com.grapes.recordstore.presentation.ui.base.BaseFragmentWithInjector
import com.grapes.recordstore.presentation.ui.screens.mainScreen.MainActivity
import kotlinx.android.synthetic.main.fragment_record_details.*


// Created by Rohyme on 10/25/2018.
class RecordsDetailsFragment : BaseFragmentWithInjector() {
    override fun getActivityVM(): Class<out ViewModel> {
        return RecordsDetailsVM::class.java
    }

    lateinit var recordModel: RecordModel
    lateinit var binding: FragmentRecordDetailsBinding
    private lateinit var viewModel: RecordsDetailsVM


    companion object {
        val RECORD_MODEL = "RECORD_MODEL"
        fun instance(recordModel: RecordModel) = RecordsDetailsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(RECORD_MODEL, recordModel)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recordModel = arguments!!.getSerializable(RECORD_MODEL) as RecordModel
        viewModel = vm as RecordsDetailsVM
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_record_details, container, false)
        binding.model = recordModel
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToggleFavListner()
        infoBt.setOnClickListener {
            binding.group.visibility = View.VISIBLE
        }
        favouriteBt.setOnClickListener {
            viewModel.toggleFav(recordModel.id)
            if (recordModel.isFavourite){
                ToastUtils.showShort("Removing from Favourite ...")
            }else{
                ToastUtils.showShort("Adding to Favourite ...")
                makeFlyAnimation((activity as MainActivity).getFavView(), favouriteBt)

            }
        }
        addToBucket.setOnClickListener {
            makeFlyAnimation((activity as MainActivity).getBucketView(), bucketImg)
            (activity as MainActivity).bucketIncrease(recordModel.bucket.numberAdded)
            recordModel.bucket.numberAdded = 1
        }
        backButton.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        increaseAddToBucket.setOnClickListener {
            recordModel.bucket.numberAdded++
        }
        decreaseAddToBucket.setOnClickListener {
            recordModel.bucket.numberAdded--
        }
        shareBt.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, "${recordModel.name}")
            startActivity(Intent.createChooser(intent, "Share Song"))
        }
    }

    /**
     * set Toggle fav listener
     */
    fun setToggleFavListner(){
        viewModel.getToggleFavListener().observe(this, Observer {
            when(it){
                is StateView.Success<*> -> {
                    val toggleModel = it.data as ToggleModel
                    recordModel.isFavourite = toggleModel.status
                }
                is StateView.Errors ->{
                    ToastUtils.showShort(it.error!!.message)
                }
            }
        })
    }

    private fun makeFlyAnimation(destView: View, targetView: View) {
        CircleAnimationUtil().attachActivity(activity).setTargetView(targetView).setMoveDuration(1000).setDestView(destView).setAnimationListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }


            override fun onAnimationEnd(animation: Animator?) {
            }

            override fun onAnimationCancel(animation: Animator?) {
            }


            override fun onAnimationStart(animation: Animator?) {
            }
        }).startAnimation()
    }


}