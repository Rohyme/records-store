package com.grapes.recordstore.presentation

import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.databinding.Observable
import android.databinding.ObservableInt
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import com.grapes.recordstore.R


// Created by Rohyme on 10/23/2018.

class SortTripleState : LinearLayout {
    private lateinit var listener: SortListener
    val STATE_OFF = -1
    val STATE_DOWN = -2
    val STATE_UP = -3
    private val stateOb: ObservableInt = ObservableInt(-1)
    private lateinit var upButton: ImageButton
    private lateinit var downButton: ImageButton

    constructor(context: Context) : super(context) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context)
    }

    fun initView(context: Context) {
//        val view = LayoutInflater.from(context).inflate(R.layout.button_triple_state_sort,this,true)
//        addView(view)/

        val inflater = context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.button_triple_state_sort, this, false)
        upButton = view.findViewById(R.id.upBt)
        downButton = view.findViewById(R.id.downBt)
        addView(view)
        setStateObs()
        setStateClickListener()
        setOffState()
    }

    private fun setStateObs() {
        stateOb.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                when (stateOb.get()) {
                    STATE_OFF -> {
                        setOffState()
                        if (this@SortTripleState::listener.isInitialized) {
                            listener.onOffSelected(this@SortTripleState)
                        }
                    }
                    STATE_UP -> {
                        setUpState()
                        if (this@SortTripleState::listener.isInitialized) {
                            listener.onUpSelected(this@SortTripleState)
                        }
                    }
                    STATE_DOWN -> {
                        setDownState()
                        if (this@SortTripleState::listener.isInitialized) {
                            listener.onDownSelected(this@SortTripleState)
                        }
                    }
                }
            }
        })
    }

    private fun setStateClickListener() {
        upButton.setOnClickListener {
            stateOb.set(STATE_UP)
        }
        downButton.setOnClickListener {
            stateOb.set(STATE_DOWN)
        }
    }

    fun executeOffState() {
        stateOb.set(STATE_OFF)
    }

    private fun setState(button: ImageButton, isActive: Boolean) {
        button.background = if (isActive) getDrawable(R.drawable.bg_sort_bt_active) else getDrawable(R.drawable.trans_bg)
    }

    private fun setUpState() {
        setState(upButton, true)
        setState(downButton, false)
    }

    private fun setDownState() {
        setState(downButton, true)
        setState(upButton, false)
    }


    private fun setOffState() {
        setState(upButton, false)
        setState(downButton, false)
    }

    private fun getDrawable(@DrawableRes drawable: Int): Drawable {
        return ContextCompat.getDrawable(context, drawable)!!
    }

     fun setSortListener(sortListener: SortListener) {
        listener = sortListener
    }

    interface SortListener {
        fun onUpSelected(v: View)
        fun onDownSelected(v: View)
        fun onOffSelected(v: View)
    }

}
