package com.grapes.recordstore.presentation.appUtils

class Constants {

  companion object {
    const val BASE_URL = "https://recordstore2.herokuapp.com/"
    const val IMG_BASEURL = ""
    const val SHARED_PREFERENCE = "SAUDI_ASSOCIATION "
  }
}

class StateConstants{
  companion object {
    const val BUTTON_LOADING = 30
    const val BUTTON_ERROR = 30
  }
}


class SortConstants{
  companion object {
    const val PRICE_SORTING = "price"
    const val NAME_SORTING = "name"
    const val DATE_SORTING = "releaseDate"
    const val ASC_SORTING = "asc"
    const val DESC_SORTING = "desc"
  }
}