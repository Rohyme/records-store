package com.grapes.recordstore.domain.useCases.favUseCases

import com.grapes.recordstore.data.remoteData.favouriteRemote.FavouriteRemoteImp
import com.grapes.recordstore.data.repositories.favouriteRepos.FavouriteRepository
import com.grapes.recordstore.domain.interactors.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

// Created by Rohyme on 10/27/2018.
class ToggleFavouriteUseCase @Inject constructor(val repository :FavouriteRepository) : SingleUseCase<Boolean, String>() {
    override fun buildUseCaseObservable(params: String?): Single<Boolean> {
        return (repository.getRemote() as FavouriteRemoteImp).toggleFavourite(params!!)
    }
}