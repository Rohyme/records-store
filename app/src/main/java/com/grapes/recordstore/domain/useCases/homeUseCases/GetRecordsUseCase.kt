package com.grapes.recordstore.domain.useCases.homeUseCases

import com.grapes.recordstore.data.remoteData.recordsRemote.RecordsRemoteImp
import com.grapes.recordstore.data.repositories.homeRepos.HomeRepository
import com.grapes.recordstore.domain.entities.RecordModel
import com.grapes.recordstore.domain.interactors.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

// Created by Rohyme on 10/23/2018.
class GetRecordsUseCase @Inject constructor(val repository: HomeRepository): SingleUseCase<ArrayList<RecordModel>, Int>() {
    override fun buildUseCaseObservable(params: Int?): Single<ArrayList<RecordModel>> {
        return (repository.getRemote() as RecordsRemoteImp).fetchRecords(params!!)
    }
}