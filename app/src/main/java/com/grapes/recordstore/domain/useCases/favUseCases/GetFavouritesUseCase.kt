package com.grapes.recordstore.domain.useCases.favUseCases

import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteModel
import com.grapes.recordstore.data.remoteData.favouriteRemote.FavouriteRemoteImp
import com.grapes.recordstore.data.repositories.favouriteRepos.FavouriteRepository
import com.grapes.recordstore.domain.interactors.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

// Created by Rohyme on 10/24/2018.
class GetFavouritesUseCase @Inject constructor(private val repository: FavouriteRepository) : SingleUseCase<ArrayList<FavouriteModel>, Void>() {
    override fun buildUseCaseObservable(params: Void?): Single<ArrayList<FavouriteModel>> {
        return (repository.getRemote() as FavouriteRemoteImp).getFavourites()

    }

}