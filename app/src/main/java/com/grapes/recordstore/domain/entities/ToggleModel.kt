package com.grapes.recordstore.domain.entities

// Created by Rohyme on 10/27/2018.
data class ToggleModel (val id:String,
                        val status: Boolean) {
}