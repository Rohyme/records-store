package com.grapes.recordstore.domain.useCases.favUseCases

import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteModel
import com.grapes.recordstore.data.remoteData.favouriteRemote.FavouriteRemoteImp
import com.grapes.recordstore.data.repositories.favouriteRepos.FavouriteRepository
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import com.grapes.recordstore.domain.interactors.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

// Created by Rohyme on 10/27/2018.
class GetSortedFavouriteUseCase  @Inject constructor(val repository: FavouriteRepository): SingleUseCase<ArrayList<FavouriteModel>, SortedRecordsRequest>() {
    override fun buildUseCaseObservable(params: SortedRecordsRequest?): Single<ArrayList<FavouriteModel>> {
    return (repository.getRemote() as FavouriteRemoteImp).getSortedFav(params!!)
    }
}