package com.grapes.recordstore.domain.useCases.homeUseCases

import com.grapes.recordstore.data.remoteData.recordsRemote.RecordsRemoteImp
import com.grapes.recordstore.data.repositories.homeRepos.HomeRepository
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import com.grapes.recordstore.domain.entities.RecordModel
import com.grapes.recordstore.domain.interactors.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

// Created by Rohyme on 10/23/2018.
class GetRecordsSortedUseCase @Inject constructor(val repository: HomeRepository): SingleUseCase<ArrayList<RecordModel>, SortedRecordsRequest>() {
    override fun buildUseCaseObservable(params: SortedRecordsRequest?): Single<ArrayList<RecordModel>> {
        return (repository.getRemote() as RecordsRemoteImp).fetchSortedRecords(params!!)
    }
}