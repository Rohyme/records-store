package com.grapes.recordstore.domain.entities

import android.databinding.BaseObservable
import android.databinding.Bindable
import carbon.BR
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class RecordModel(
        @SerializedName("id") var id: String = "",
        @SerializedName("name") var name: String = "",
        @SerializedName("price") var price: Double = 0.0,
        @SerializedName("artist") var artist: String = "",
        @SerializedName("favourite") var _isFavourite: Boolean = false,
        @SerializedName("img") var img: String = "",
        @SerializedName("desc") var desc: String = "",
        @SerializedName("bucket") var bucket: Bucket = Bucket(),
        @SerializedName("releaseDate") var releaseDate: String = "",
        @SerializedName("label") var label: String = ""
):Serializable , BaseObservable(){

    var isFavourite :Boolean
        @Bindable get() = _isFavourite
        set(value) {
            _isFavourite = value
            notifyPropertyChanged(BR.favourite)
        }


     class Bucket(
            @SerializedName("addToBucket") var addToBucket: Boolean = false,
            var _numberAdded :Int=1
     ):Serializable, BaseObservable() {
          var numberAdded :Int
              @Bindable get() = _numberAdded
              set(value) {
                  _numberAdded = value
                  notifyPropertyChanged(BR.numberAdded)
              }

     }
}