package com.grapes.recordstore.data.cacheData.favouriteCache

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

// Created by Rohyme on 10/24/2018.

@Entity(tableName = "favourites")
class FavouriteModel(
        @PrimaryKey @ColumnInfo(name = "id") val id: String,
        @ColumnInfo(name = "img") val img: String,
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "releaseDate") val releaseDate: String,
        @ColumnInfo(name = "artist") val artist: String,
        @ColumnInfo(name = "price") val price: Double
)
