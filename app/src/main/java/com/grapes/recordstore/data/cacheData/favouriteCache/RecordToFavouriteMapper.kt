package com.grapes.recordstore.data.cacheData.favouriteCache

import com.grapes.recordstore.domain.base.BaseMapper
import com.grapes.recordstore.domain.entities.RecordModel
import javax.inject.Inject

// Created by Rohyme on 10/24/2018.

class RecordToFavouriteMapper @Inject constructor() : BaseMapper<RecordModel, FavouriteModel> {
    override fun fromEntity(e: RecordModel): FavouriteModel {
        return FavouriteModel(
                e.id, e.img, e.name, e.releaseDate, e.artist, e.price
        )
    }

    override fun toEntity(t: FavouriteModel): RecordModel {
        return RecordModel()
    }
}