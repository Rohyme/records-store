package com.grapes.recordstore.data.remoteData.recordsRemote

import com.grapes.recordstore.data.service.ApiService
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import com.grapes.recordstore.domain.entities.RecordModel
import io.reactivex.Single
import javax.inject.Inject

// Created by Rohyme on 10/23/2018.
class RecordsRemoteImp @Inject constructor(val apiService: ApiService) : RecordsRemoteI {
    override fun fetchRecords(page: Int): Single<ArrayList<RecordModel>> {
        return apiService.getRecords(page)
    }

    override fun fetchSortedRecords(sortBody: SortedRecordsRequest): Single<ArrayList<RecordModel>> {
        return apiService.getRecordsSorted(sortBody.page, sortBody.sortType, sortBody.isDesc)
    }
}