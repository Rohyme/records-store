package com.grapes.recordstore.data.cacheData.favouriteCache

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

// Created by Rohyme on 10/24/2018.

@Database(entities = [FavouriteModel::class], version = 1)
abstract class FavouriteDataBase : RoomDatabase() {
    abstract fun favouriteDao(): FavouriteDao
}