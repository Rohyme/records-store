package com.grapes.recordstore.data.remoteData.favouriteRemote

import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteModel
import com.grapes.recordstore.data.cacheData.favouriteCache.RecordToFavouriteMapper
import com.grapes.recordstore.data.service.ApiService
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import io.reactivex.Single
import javax.inject.Inject

// Created by Rohyme on 10/27/2018.
class FavouriteRemoteImp @Inject constructor(val apiService: ApiService,
                                             val recorderMapper: RecordToFavouriteMapper) : FavouriteRemoteI {
    override fun getFavourites(): Single<ArrayList<FavouriteModel>> {
        return apiService.getFavourites().map { recordsList ->
            ArrayList(recordsList.map {
                recorderMapper.fromEntity(it)
            })
        }
    }

    override fun toggleFavourite(recordId: String): Single<Boolean> {
        return apiService.toggleFav(recordId)
    }

    override fun getSortedFav(sortedFav: SortedRecordsRequest): Single<ArrayList<FavouriteModel>> {
        return apiService.getSortedFav(sortedFav.sortType, sortedFav.isDesc).map { recordsList ->
            ArrayList(recordsList.map {
                recorderMapper.fromEntity(it)
            })
        }
    }
}