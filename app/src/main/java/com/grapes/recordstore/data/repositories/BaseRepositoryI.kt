package com.grapes.recordstore.data.repositories

import com.grapes.recordstore.data.cacheData.BaseCacheI
import com.grapes.recordstore.data.remoteData.BaseRemoteI

// Created by Rohyme on 10/23/2018.
interface BaseRepositoryI {
    fun getRemote():BaseRemoteI?{
        return null
    }
    fun getCache(): BaseCacheI?{
        return null
    }
}