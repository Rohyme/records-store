package com.grapes.recordstore.data.remoteData.favouriteRemote

import com.grapes.recordstore.data.cacheData.favouriteCache.FavouriteModel
import com.grapes.recordstore.data.remoteData.BaseRemoteI
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import io.reactivex.Single

// Created by Rohyme on 10/27/2018.
interface FavouriteRemoteI : BaseRemoteI {
    fun getFavourites(): Single<ArrayList<FavouriteModel>>
    fun toggleFavourite(recordId :String): Single<Boolean>
    fun getSortedFav(sortedFav: SortedRecordsRequest): Single<ArrayList<FavouriteModel>>
}