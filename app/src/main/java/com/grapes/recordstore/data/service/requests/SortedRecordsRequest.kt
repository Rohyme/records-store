package com.grapes.recordstore.data.service.requests

// Created by Rohyme on 10/23/2018.
class SortedRecordsRequest(val page: Int= 1,
                           val sortType: String,
                           val isDesc: Boolean) {
}