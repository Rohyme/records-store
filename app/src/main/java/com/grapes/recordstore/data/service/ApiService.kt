package com.grapes.recordstore.data.service

import com.grapes.recordstore.domain.entities.RecordModel
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {

    @GET("records")
    fun getRecords(@Query(value = "page", encoded = true) page: Int): Single<ArrayList<RecordModel>>

    @GET("sortedRecords")
    fun getRecordsSorted(@Query(value = "page", encoded = true) page: Int,
                         @Query(value = "sortType", encoded = true) sortType: String,
                         @Query(value = "isDesc", encoded = true) isDesc: Boolean): Single<ArrayList<RecordModel>>

    @FormUrlEncoded
    @POST("toggleFav")
    fun toggleFav(@Field("recordId") recordId: String): Single<Boolean>

    @GET("favourites")
    fun getFavourites(): Single<ArrayList<RecordModel>>

    @GET("sortedFavourites")
    fun getSortedFav(@Query(value = "sortType", encoded = true) sortType: String,
                     @Query(value = "isDesc", encoded = true) isDesc: Boolean): Single<ArrayList<RecordModel>>

}