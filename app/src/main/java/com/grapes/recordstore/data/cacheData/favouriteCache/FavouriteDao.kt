package com.grapes.recordstore.data.cacheData.favouriteCache

import android.arch.persistence.room.*
import io.reactivex.Single

// Created by Rohyme on 10/24/2018.

@Dao
interface FavouriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavourite(favouriteModel: FavouriteModel): Long

    @Delete
    fun removeFromFavourite(favouriteModel: FavouriteModel): Int

    @Query("SELECT * FROM favourites")
    fun getFavourites(): Single<List<FavouriteModel>>

    @Query("SELECT * FROM favourites ORDER BY releaseDate ASC ")
    fun getFavouritesSortedByDateASC(): Single<List<FavouriteModel>>

    @Query("SELECT * FROM favourites ORDER BY price ASC ")
    fun getFavouritesSortedByPriceASC(): Single<List<FavouriteModel>>

    @Query("SELECT * FROM favourites ORDER BY name ASC ")
    fun getFavouritesSortedByNameASC(): Single<List<FavouriteModel>>

    @Query("SELECT * FROM favourites ORDER BY price DESC")
    fun getFavouritesSortedByPriceDESC(): Single<List<FavouriteModel>>

    @Query("SELECT * FROM favourites ORDER BY name DESC")
    fun getFavouritesSortedByNameDESC(): Single<List<FavouriteModel>>

    @Query("SELECT * FROM favourites ORDER BY releaseDate DESC")
    fun getFavouritesSortedByDateDESC(): Single<List<FavouriteModel>>

}