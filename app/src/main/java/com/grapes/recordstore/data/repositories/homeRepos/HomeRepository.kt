package com.grapes.recordstore.data.repositories.homeRepos

import com.grapes.recordstore.data.remoteData.BaseRemoteI
import com.grapes.recordstore.data.remoteData.recordsRemote.RecordsRemoteImp
import com.grapes.recordstore.data.repositories.BaseRepositoryI
import javax.inject.Inject

// Created by Rohyme on 10/23/2018.
class HomeRepository @Inject constructor(val remoteRecords : RecordsRemoteImp) : BaseRepositoryI {

    override fun getRemote(): BaseRemoteI {
        return remoteRecords
    }
}