package com.grapes.recordstore.data.remoteData.recordsRemote

import com.grapes.recordstore.data.remoteData.BaseRemoteI
import com.grapes.recordstore.data.service.requests.SortedRecordsRequest
import com.grapes.recordstore.domain.entities.RecordModel
import io.reactivex.Single

// Created by Rohyme on 10/27/2018.
interface RecordsRemoteI : BaseRemoteI {
    fun fetchRecords(page :Int) : Single<ArrayList<RecordModel>>
    fun fetchSortedRecords(sortBody : SortedRecordsRequest): Single<ArrayList<RecordModel>>
}