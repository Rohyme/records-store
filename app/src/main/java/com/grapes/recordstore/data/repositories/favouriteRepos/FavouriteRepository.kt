package com.grapes.recordstore.data.repositories.favouriteRepos

import com.grapes.recordstore.data.remoteData.BaseRemoteI
import com.grapes.recordstore.data.remoteData.favouriteRemote.FavouriteRemoteImp
import com.grapes.recordstore.data.repositories.BaseRepositoryI
import javax.inject.Inject

// Created by Rohyme on 10/24/2018.
class FavouriteRepository @Inject constructor(val favouriteRemote: FavouriteRemoteImp)  :BaseRepositoryI{

    override fun getRemote(): BaseRemoteI? {
        return favouriteRemote
    }
}