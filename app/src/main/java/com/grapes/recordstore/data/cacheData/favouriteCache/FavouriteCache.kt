package com.grapes.recordstore.data.cacheData.favouriteCache

import com.grapes.recordstore.data.cacheData.BaseCacheI
import com.grapes.recordstore.domain.entities.RecordModel
import com.grapes.recordstore.presentation.appUtils.SortConstants
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

// Created by Rohyme on 10/24/2018.

class FavouriteCache @Inject constructor(val dao: FavouriteDao,
                                         val mapper: RecordToFavouriteMapper) : BaseCacheI {

    fun addToFavourite(favouriteModel: RecordModel): Single<Long> {
        return Observable.fromCallable {
            dao.insertFavourite(mapper.fromEntity(favouriteModel))
        }.singleOrError()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun removeFromFavourite(favouriteModel: FavouriteModel): Single<Int> {
        return Observable.fromCallable {
            dao.removeFromFavourite(favouriteModel)
        }.singleOrError()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getAllFav(): Single<ArrayList<FavouriteModel>> {
        return dao.getFavourites().map {
            ArrayList(it)
        }
    }

    fun getFavSortedAsc(sortType: String): Single<ArrayList<FavouriteModel>> {
        return when (sortType) {
            SortConstants.NAME_SORTING -> {
                dao.getFavouritesSortedByNameASC()
                        .map {
                            ArrayList(it)
                        }
            }
            SortConstants.PRICE_SORTING -> {
                dao.getFavouritesSortedByPriceASC()
                        .map {
                            ArrayList(it)
                        }
            }
            SortConstants.DATE_SORTING -> {
                dao.getFavouritesSortedByDateASC()
                        .map {
                            ArrayList(it)
                        }
            }
            else -> {
                dao.getFavouritesSortedByNameASC()
                        .map {
                            ArrayList(it)
                        }
            }

        }

    }

    fun getFavSortedDesc(sortType: String): Single<ArrayList<FavouriteModel>> {
        return when (sortType) {
            SortConstants.NAME_SORTING -> {
                dao.getFavouritesSortedByNameDESC()
                        .map {
                            ArrayList(it)
                        }
            }
            SortConstants.PRICE_SORTING -> {
                dao.getFavouritesSortedByPriceDESC()
                        .map {
                            ArrayList(it)
                        }
            }
            SortConstants.DATE_SORTING -> {
                dao.getFavouritesSortedByDateDESC()
                        .map {
                            ArrayList(it)
                        }
            }
            else -> {
                dao.getFavouritesSortedByNameASC()
                        .map {
                            ArrayList(it)
                        }
            }

        }
    }
}